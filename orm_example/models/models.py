from pydantic import BaseModel

class Token(BaseModel):
    access_token: str
    token_type: str = "Bearer"

class ItemBase(BaseModel):
    title: str
    description: str | None = None

class ItemCreate(ItemBase):
    pass

class ItemDomain(ItemBase):
    owned_id: int
    id: int

    class Config:
        orm_mode = True

class UserBase(BaseModel):
    email:str

class UserCreate(UserBase):
    password: str

class UserDomain(UserBase):
    id: int
    items: list[ItemDomain] = []

    class Config:
        orm_mode=True