from sqlalchemy.orm import Session
from passlib.context import CryptContext

from models import User, Item, UserCreate, ItemCreate

pwd_context = CryptContext(["bcrypt"])

def verify_pass(plain_password, hashed_password) -> bool:
    salt_password = plain_password + "magic key"
    return pwd_context.verify(salt_password, hashed_password)

def hash_passowd(password)->str:
    return pwd_context.hash(password + "magic key") 

def get_users(db: Session) ->list[User]:
    return db.query(User).all()

def get_user_by_email(db: Session, email:str) -> User:
    return db.query(User).filter(User.email == email).first()

def create_user(db: Session, user:UserCreate) -> User:
    hashed_password = hash_passowd(user.password)
    db_user = User(email=user.email, password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def get_items(db: Session) ->list[Item]:
    return db.query(Item).all()

def create_user_item(db: Session, item:ItemCreate, user_id: int) -> User:
    db_item = Item(title=item.title, description=item.description, owned_id = user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item