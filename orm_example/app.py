from fastapi import FastAPI, Depends, HTTPException, APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from typing import Annotated
from datetime import timedelta, datetime
from jose import jwt, JWTError

from sqlalchemy.orm import Session

import models, repository


from models.database import SessionLocal

app = FastAPI()

SECRET_KEY = "Very0long0magic0secrete0key"
ALGORITHM_HASH = "HS256"
TOKEN_EXPIRE_Minutes = 1

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
# security = HTTPBasic()

def get_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

prefix_router = APIRouter(prefix="/api")

def generate_token( data: dict, expire_delta: timedelta)->str:
    expire_time = datetime.utcnow() + expire_delta
    data = data | {"expire": expire_time.strftime("%m/%d/%Y, %H:%M:%S")}
    return jwt.encode(data, algorithm=ALGORITHM_HASH, key=SECRET_KEY)

@prefix_router.post("/login/", response_model=models.Token)
def user_auth(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    db: Session = Depends(get_session)
    ):
    user = repository.get_user_by_email(db, form_data.username)
    if user:
        if repository.verify_pass(form_data.password, user.password):
            return models.Token(
                access_token=generate_token({"sub": user.email}, timedelta(minutes=TOKEN_EXPIRE_Minutes))
            )
    raise HTTPException(status_code=401, detail="Invalid username or password")

@prefix_router.post("/users/", response_model=models.UserDomain)
def create_user(user: models.UserCreate, db: Session = Depends(get_session)):
    db_user = repository.get_user_by_email(db, user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="user exists")
    return repository.create_user(db, user)


@prefix_router.get("/users/", response_model=list[models.UserDomain])
def get_users(db: Session = Depends(get_session)):
    return repository.get_users(db)

@prefix_router.get("/user/me", response_model=models.UserDomain)
def get_user_email(token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_session)):
    token_data = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM_HASH)
    email = token_data.get("sub")
    db_user =  repository.get_user_by_email(db, email)
    if not db_user:
        raise HTTPException(status_code=404, detail="user not found")
    return db_user

@prefix_router.get("/items/", response_model=list[models.ItemDomain])
def get_items(db: Session = Depends(get_session)):
    return repository.get_items(db)

@prefix_router.post("/items/{user_id}", response_model=models.ItemDomain)
def create_user(user_id:int, item: models.ItemCreate, db: Session = Depends(get_session)):
    return repository.create_user_item(db, item, user_id)

@prefix_router.get("/users/items/{email}", response_model=list[models.ItemDomain])
def get_user_items(email:str, db: Session = Depends(get_session)):
    db_user =  repository.get_user_by_email(db, email)
    if not db_user:
        raise HTTPException(status_code=404, detail="user not found")
    return db_user.items

app.include_router(prefix_router)