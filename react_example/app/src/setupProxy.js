const { createProxyMidlleawre} = require('http-proxy-middleware');

module.exports = function(app){
    app.use('/api', createProxyMidlleawre({target: 'http://127.0.0.1:8000', changeOrigin: true}));
}