import uuid

from pydantic import BaseModel, Field, field_serializer


class LinkAPIModel(BaseModel):
    url: str


class ShortLinkModel(BaseModel):
    url: str
    uuid: str = Field(default_factory=lambda: uuid.uuid4().hex)


class ShortLinkAPIModel(BaseModel):
    url: str
    short_link: str = Field(validation_alias="uuid")

    @field_serializer("short_link", when_used="json")
    def generate_short_link(self, uuid, **args):
        return f"http://localhost:8000/link/{uuid}"
