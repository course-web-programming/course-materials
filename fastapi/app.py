from models import LinkAPIModel, ShortLinkAPIModel, ShortLinkModel

from fastapi import APIRouter, FastAPI
from fastapi.responses import RedirectResponse

app = FastAPI()
api_router = APIRouter(prefix="/api/v1")

base: dict[str, ShortLinkModel] = {}


@api_router.post("/short")
async def short_link_gen(link: LinkAPIModel) -> ShortLinkAPIModel:
    slm = ShortLinkModel(url=link.url)
    base[slm.uuid] = slm
    return ShortLinkAPIModel(**slm.model_dump())


@app.get("/link/{uuid}")
async def redirect(uuid: str) -> RedirectResponse:
    slm = base[uuid]
    return RedirectResponse(url=slm.url)


app.include_router(api_router)
