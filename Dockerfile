FROM python:3.11-slim

WORKDIR /app

COPY orm_example/requiremets.txt .
RUN pip install -r requiremets.txt

COPY orm_example/* ./

CMD ["alembic", "upgrade", "head"]

ENTRYPOINT [ "uvicorn", "app:app" ,"--reload","--host=0.0.0.0", "--port=8000" ]