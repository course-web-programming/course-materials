import socketserver
import threading
from queue import Queue
import io

SERVER_ADDRESS = ("localhost", 8686)
server_queue = Queue(10)
connections_pull: list[tuple[tuple[str,int], io.BufferedIOBase]] = []
EMPTY_LINE = b''
SPLIT_SYM = b'\n'

def send_from_queue(connection_pull_: list[tuple[tuple[str,int], io.BufferedIOBase]], queue: Queue) -> None:
    while True:
        seneder_address, data = queue.get()
        for address, conn in connection_pull_:
            if address != seneder_address:
                print("try to send: ", address, " data: ", data)
                try:
                    conn.write(data)
                except OSError:
                    connection_pull_ = [( addr, connection)for addr, connection  in connection_pull_ if addr != address]
        queue.task_done()


thread_write = threading.Thread(
    target=send_from_queue, args=(connections_pull, server_queue)
)
thread_write.start()

class MyHandler(socketserver.StreamRequestHandler):

    def __init__(self, *args,connections_pull_:list=connections_pull, queue: Queue=server_queue, **kwrags):
        self.connection_pull = connections_pull_
        self.queue = queue
        super().__init__(*args, **kwrags)
    
    def handle(self) -> None:
        print(self.client_address)
        self.connection_pull.append((self.client_address, self.wfile))
        line = EMPTY_LINE
        while data_byte := self.rfile.read(1):
            if data_byte == SPLIT_SYM:
                print("recive: ",line)
                self.queue.put((self.client_address, line))
                line = EMPTY_LINE
                continue
            line += data_byte


with socketserver.ThreadingTCPServer(SERVER_ADDRESS, MyHandler) as server:
    server.timeout = 10
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()
        thread_write.join()