import socket
import threading
from queue import Queue
from typing import List, Tuple

# Set server address
SERVER_ADDRESS = ("127.0.0.1", 8000)

server_queue = Queue(10)
history: List[str] = []
connections: List[Tuple[socket.socket, str]] = []

# Configure socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(SERVER_ADDRESS)
server_socket.listen(10)
print("server is running, please, press ctrl+c to stop")


def wait_connection(server: socket.socket):
    while True:
        connection, address = server.accept()
        send_history(connection)
        print(f"new connection from {address}")
        yield (connection, address)


def wait_message(connection: socket.socket) -> bytes:
    while True:
        data = connection.recv(1024)
        if not data:
            break
        yield data


def read_message(connection: socket.socket, queue: Queue, address: str) -> None:
    try:
        for data in wait_message(connection):
            print(f"Client {address} sent: {data}")
            queue.put((address, data))
            history.append(f"{address}: {data.decode()}")
    finally:
        notify_all_about_disconnection(address)
        remove_connection(connection, address)


def send_from_queue(connection_pul_: list[socket.socket], queue: Queue) -> None:
    while True:
        seneder_address, data = queue.get()
        for conn, address in connection_pul_:
            if address != seneder_address:
                conn.send(data)
        queue.task_done()


def send_history(connection: socket.socket) -> None:
    for data in history:
        connection.sendall(data.encode())


def notify_all_about_new_member(
    connection_pul_: list[socket.socket], new_address
) -> None:
    message = f"{new_address} connected to this chat."
    for conn, _ in connection_pul_:
        conn.sendall(message.encode())


def notify_all_about_disconnection(disconnected_address: str) -> None:
    message = f"{disconnected_address} disconnected from the chat."
    for conn, address in connections:
        if address != disconnected_address:
            conn.sendall(message.encode())


def remove_connection(connection: socket.socket, address: str) -> None:
    global connections
    connections = [(conn, addr) for conn, addr in connections if addr != address]
    connection.close()


thread_write = threading.Thread(
    target=send_from_queue, args=(connections, server_queue)
)
thread_write.start()

for connection, address in wait_connection(server_socket):
    notify_all_about_new_member(connections, str(address))
    connections.append((connection, str(address)))
    thread_read = threading.Thread(
        target=read_message, args=(connection, server_queue, str(address))
    )
    thread_read.start()
