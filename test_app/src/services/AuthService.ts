import type {AxiosInstance} from "axios";
import type {User, AuthedUser} from "../classes";
import apiClient from "./ApiClient";

class AuthService {
    private api: AxiosInstance;
    constructor (){ 
        this.api = apiClient;
    }

    async login(data: User): Promise<AuthedUser> {
        console.log(data)
        const response = await this.api.post("/api/v1/login", data);
        const token = response.data.access_token;
        console.log(token);
        const authedUser: AuthedUser = {
            username: data.username,
            access_token: token
        };
        console.log(authedUser);
        return authedUser;
    }
}

export default new AuthService();