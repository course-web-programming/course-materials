export interface User {
    username: string;
    password: string;
};
export interface AuthedUser {
    access_token: string;
    username: string;
};