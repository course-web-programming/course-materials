class USERModel:
    id: int
    name: str

    def __init__(self, id_, name_) -> None:
        self.id = id_
        self.name = name_


database: list[USERModel] = [USERModel(0, "new")]


class DataBaseUSERModel(USERModel):
    @classmethod
    def from_model(cls, model: USERModel):
        return cls(model.id, model.name)

    @classmethod
    def get_all_models(cls) -> list["DataBaseUSERModel"]:
        return [cls.from_model(model) for model in database]

    @classmethod
    def get_model_by_id(cls, id: int) -> "DataBaseUSERModel":
        return cls.from_model(database[id])

    def update(self, **kwrags) -> "DataBaseUSERModel":
        for field, value in kwrags.items():
            setattr(self, field, value)
        database[self.id] = self
        return self


class RequestUSERModel(USERModel):
    @classmethod
    def from_form(cls, name: str):
        return cls(None, name)

    def save(self) -> DataBaseUSERModel:
        database.append(self)
        self.id = len(database) - 1
