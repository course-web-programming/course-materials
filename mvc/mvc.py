from flask import Flask, request
from models import *
from views import *

app = Flask(__name__)


@app.route("/", methods=["GET"])
def get_users():
    users = DataBaseUSERModel.get_all_models()
    return UsersPageView("POST", "/", ModelsView(users)).generate()


@app.route("/", methods=["POST"])
def create_user():
    new_user = RequestUSERModel.from_form(request.form["name"])
    new_user.save()
    users = DataBaseUSERModel.get_all_models()
    return UsersPageView("POST", "/", ModelsView(users)).generate()


@app.route("/<int:id>", methods=["GET"])
def get_user(id):
    user = DataBaseUSERModel.get_model_by_id(id)
    return UsersPageView("POST", f"/{id}", ModelView(user)).generate()


@app.route("/<int:id>", methods=["POST"])
def change_user(id):
    user = DataBaseUSERModel.get_model_by_id(id)
    user.update(**request.form)
    return UsersPageView("POST", f"/{id}", ModelView(user)).generate()


if __name__ == "__main__":
    app.run("127.0.0.1", 8080, debug=True)
