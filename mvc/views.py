from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from models import Model

class View:
    def generate(self) -> str:
        return ""

class ModelView(View):
    def __init__(self, model: "Model"):
        self.model = model

    def generate(self) -> str:
        return f"id={self.model.id}, name=<a href={self.model.id}>{self.model.name}</a>"

class ModelsView(View):
    def __init__(self, models:list["Model"]):
        self.models = models

    def generate(self) -> str:
        return "".join([f"<li>{ModelView(model).generate()}</li>" for model in self.models])

class UsersPageView(View):
    def __init__(self, method_:str, path_:str, content_view:ModelsView):
        self.method = method_
        self.path = path_
        self.content_view = content_view
        
    def generate(self)->str:
        return f"""
<html>
<head></head>
<body>
<h1>UsersPage</h1>
{self.content_view.generate()}
<form action="{self.path}" method="{self.method}">
  <label for="name">Name</label><br>
  <input type="text" id="name" name="name" value="John"><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>
"""