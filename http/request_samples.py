import pprint as pp

import requests
from requests import Response


def response_print(resp: Response):
    print(
        f"\n========================================\n{resp.request.method} response\n"
    )
    pp.pprint(resp.status_code)
    pp.pprint(resp.url)
    pp.pprint(resp.headers)
    pp.pprint(resp.content)
    if resp.content:
        pp.pprint(resp.json())


get_resp = requests.get("https://httpbin.org/get?request=true&hello=world")
response_print(get_resp)

post_resp = requests.post(
    "https://httpbin.org/post?request=true&hello=world",
    data={"key": "value"},
    # json={"key": "value"},
    # headers={"Content-Type": "application/json"},
)
response_print(post_resp)

put_resp = requests.put("https://httpbin.org/get", data={"key": "value"})
response_print(put_resp)

patch_resp = requests.patch("https://httpbin.org/patch", data={"key": "value"})
response_print(patch_resp)

delete_resp = requests.delete("https://httpbin.org/delete")
response_print(delete_resp)

options_resp = requests.options("https://httpbin.org/get")
response_print(options_resp)

trace_resp = requests.request("TRACE", "https://vk.com/")
response_print(trace_resp)


payload = {"key1": "value1", "key2": ["value2", "value3"]}
get_resp = requests.get("http://httpbin.org/get", params=payload)
response_print(get_resp)
