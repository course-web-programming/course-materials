import http.server
import socketserver

PORT = 8081

Handler = http.server.SimpleHTTPRequestHandler


class CustomHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, "OK")
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("hello world".encode("UTF-8"))
        self.wfile.write("HELLO WORLD".encode("UTF-8"))


with socketserver.TCPServer(("0.0.0.0", PORT), CustomHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()

# with open("index.html", "r") as f:
#     print(f.read())
