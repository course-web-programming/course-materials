import datetime as dt
from typing import Annotated

from jose import jwt
from models import (
    LinkAPIModel,
    SessionDep,
    ShortLinkAPIModel,
    ShortLinkDBModel,
    Token,
    UserAPIModel,
    UserDBModel,
)
from passlib.context import CryptContext
from sqlmodel import Session, select

from fastapi import APIRouter, Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse, Response
from fastapi.security import OAuth2PasswordBearer

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
api_router = APIRouter(prefix="/api/v1")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/v1/login")
SECRETE_KEY = "AnySecreteKey..12312435342"
ALGORITHM_HASH = "HS256"
TOKEN_EXPIRE_MINUTES = 1

pwd_context = CryptContext(["bcrypt"])


def generate_token(data: dict, expire_time: dt.timedelta) -> str:
    expire_time: dt.datetime = dt.datetime.now() + expire_time
    data.update({"expire": expire_time.strftime("%m/%d/%Y %H:%M:%S")})
    return jwt.encode(
        data,
        SECRETE_KEY,
        algorithm=ALGORITHM_HASH,
    )


def get_user_by_name(
    username: str,
    session: Session,
) -> UserDBModel:
    return session.exec(
        select(UserDBModel).where(UserDBModel.username == username)
    ).first()


def verify_password(plain_passowrd: str, hash_password: str) -> bool:
    salt_password = plain_passowrd + "_salt"
    return pwd_context.verify(salt_password, hash_password)


def hash_password(plain_passowrd: str) -> str:
    return pwd_context.hash(plain_passowrd + "_salt")


@api_router.post("/register")
def create_user(
    user: UserAPIModel,
    session: SessionDep,
) -> UserDBModel:
    user_db = get_user_by_name(user.username, session)
    if user_db:
        raise HTTPException(400, "Not unique username.")
    user_db = UserDBModel(username=user.username, password=hash_password(user.password))
    session.add(user_db)
    session.commit()
    session.refresh(user_db)
    return user_db


@api_router.post("/login")
def auth_user(
    form_data: UserAPIModel,
    session: SessionDep,
) -> Token:
    user_db = get_user_by_name(form_data.username, session)
    if user_db:
        if verify_password(form_data.password, user_db.password):
            return Token(
                access_token=generate_token(
                    {"sub": user_db.username},
                    dt.timedelta(minutes=TOKEN_EXPIRE_MINUTES),
                )
            )
    raise HTTPException(401, "Wrong password or username.")


@api_router.get("/users/me")
def get_user(
    token: Annotated[str, Depends(oauth2_scheme)], session: SessionDep
) -> UserDBModel:
    token_data = jwt.decode(token, SECRETE_KEY, algorithms=ALGORITHM_HASH)
    username = token_data["sub"]
    user_db = get_user_by_name(username=username, session=session)
    if not user_db:
        raise HTTPException(status_code=404, detail="no such user.")
    return user_db


@api_router.post("/short")
async def short_link_gen(
    link: LinkAPIModel,
    session: SessionDep,
) -> ShortLinkAPIModel:
    slm = ShortLinkDBModel(url=link.url)
    session.add(slm)
    session.commit()
    session.refresh(slm)
    return ShortLinkAPIModel(**slm.model_dump())


@api_router.delete("/delete/{_id}")
async def short_link_delete(_id: int, session: SessionDep) -> ShortLinkAPIModel:
    slm = session.get(ShortLinkDBModel, _id)
    if not slm:
        raise HTTPException(status_code=404, detail="Short link not found")
    session.delete(slm)
    session.commit()
    return Response(status_code=201)


@app.get("/link/{_id}")
async def redirect(_id: int, session: SessionDep) -> RedirectResponse:
    slm = session.get(ShortLinkDBModel, _id)
    if not slm:
        raise HTTPException(status_code=404, detail="Short link not found")
    return RedirectResponse(url=slm.url)


app.include_router(api_router)
