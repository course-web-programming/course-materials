from pydantic import BaseModel, Field, field_serializer


class LinkAPIModel(BaseModel):
    url: str


class ShortLinkAPIModel(BaseModel):
    url: str
    short_link: int = Field(validation_alias="id")

    @field_serializer("short_link", when_used="json")
    def generate_short_link(self, _id, **args):
        return f"http://localhost:8000/link/{_id}"


class UserAPIModel(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
