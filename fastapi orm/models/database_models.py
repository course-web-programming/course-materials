from typing import Union

from sqlmodel import Field, SQLModel


class ShortLinkDBModel(SQLModel, table=True):
    url: str
    id: Union[int, None] = Field(default=None, primary_key=True)


class UserDBModel(SQLModel, table=True):
    id: Union[int, None] = Field(default=None, primary_key=True)
    username: str = Field(unique=True, index=True)
    password: str
