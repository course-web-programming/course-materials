from typing import Annotated

from settings import DBSettings
from sqlmodel import Session, SQLModel, create_engine

from fastapi import Depends

db_settings = DBSettings()
print(db_settings.url)
engine = create_engine(db_settings.url, connect_args={"check_same_thread": False})


def create_all_tables():
    SQLModel.metadata.create_all(engine)


def get_seesion() -> Session:
    with Session(engine) as session:
        yield session


SessionDep = Annotated[Session, Depends(get_seesion)]
