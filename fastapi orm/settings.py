from typing import Union

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class DBSettings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="DATA_BASE_", env_file=".env", case_sensitive=False
    )
    connection_type: str = Field(default="sqlite")
    connection_url: str = Field(default="test.db")
    username: Union[str, None] = Field(default=None)
    password: Union[str, None] = Field(default=None)
    database_name: Union[str, None] = Field(default=None)

    @property
    def url(self):
        if self.connection_type == "sqlite":
            return f"{self.connection_type}:///{self.connection_url}"
        if self.connection_type == "pgsql":
            return f"{self.connection_type}:///{self.username}:{self.password}@{self.connection_url}/{self.database_name}"
